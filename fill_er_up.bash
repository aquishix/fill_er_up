#!/bin/bash
echo ""
if [ -z "$1" ]
then
	echo "Usage:"
	echo "fill_er_up.bash FILELENGTH [NUMFILES]"
	exit 1
fi
length=$1

if (( $length < 1 )) 
then
	echo "FILELENGTH must be at least 1."
	exit 1
fi

if [ -z "$2" ]
then
	echo "NUMFILES unspecified; defaulting to 1."
	num_files=1
else
	num_files=$2
fi

if (( $num_files < 1 ))
then
	echo "NUMFILES must be at least 1."
	exit 1 
fi

if [ ! -d "random_files" ]
then
	echo "Creating random_files/ directory..."
	mkdir random_files
fi

echo "Creating $num_files file(s) of length $length..."
chunk_size=$[length/10]

if (( $length <  10 ))
then
	num_chunks=$length
else
	num_chunks=10
fi


echo "num_chunks == $num_chunks"
echo "chunk_size == $chunk_size"

residual=$[$length - (chunk_size * 10)]
#echo "residual is $residual"

for i in `seq 1 $num_files`;
do
	head -c $residual </dev/urandom >>random_files/$i.txt	
	for j in `seq 1 $num_chunks`;
	do
		head -c $chunk_size </dev/urandom >>random_files/$i.txt
		echo -n "."
	done
	
	echo "File $i.txt written to disk."
done

echo ""
